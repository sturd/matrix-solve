package matrix_solve

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type matrixRow struct {
	vals []float64
}

func (r *matrixRow) numCols() int {
	return len(r.vals)
}

type Matrix struct {
	rows []matrixRow
}

func (r *Matrix) NumRows() int {
	return len(r.rows)
}

func (r *Matrix) NumCols() int {
	return r.rows[0].numCols()
}

func NewMatrix(inputData string) (*Matrix, error) {
	err := validInputString(inputData)
	if err != nil {
		return nil, err
	}

	m := &Matrix{}
	dimOpen := -1
	for i, c := range inputData {
		if c == '{' && i > 0 {
			dimOpen = i
		}
		if c == '}' && i < len(inputData)-1 {
			r, err := parseRow(inputData[dimOpen:i+1])
			if err != nil {
				return nil, err
			}
			m.rows = append(m.rows, *r)
		}
	}
	return m, nil
}

func parseRow(inputData string) (*matrixRow, error) {
	err := validInputString(inputData)
	if err != nil {
		return nil, err
	}
	rowData := inputData[1:len(inputData)-1]
	rowData = strings.ReplaceAll(rowData, " ", "")
	cols := strings.Split(rowData, ",")

	row := &matrixRow{}
	for _, col := range cols {
		floatVal, err := strconv.ParseFloat(col, 64)
		if err != nil {
			return nil, err
		}
		row.vals = append(row.vals, floatVal)
	}
	return row, nil
}

func validInputString(inputData string) error {
	if inputData[0] != '{' || inputData[len(inputData)-1] != '}' {
		return errors.New("invalid matrix declaration string")
	}
	return nil
}

func (m *Matrix) Val(i, j int) (float64, error) {
	if i > m.NumRows() {
		return 0.0, errors.New(fmt.Sprintf("row %d does not exist", i))
	}
	if j > m.NumCols() {
		return 0.0, errors.New(fmt.Sprintf("column %d does not exist", j))
	}
	return m.rows[i].vals[j], nil
}

func (m *Matrix) Multiply(b *Matrix) (*Matrix, error) {
	if m.NumCols() != b.NumRows() {
		return nil, errors.New("matrix 'A' column count does not equal matrix 'b' row count")
	}

	c := &Matrix{}
	for i := 0; i < m.NumRows(); i++ {
		c.rows = append(c.rows, matrixRow{})
		for j := 0; j < b.NumCols(); j++ {
			c.rows[i].vals = append(c.rows[i].vals, 0.0)
			for k := 0; k < m.NumCols(); k++ {
				vA := m.rows[i].vals[k]
				vB := b.rows[k].vals[j]
				c.rows[i].vals[j] += vA * vB
			}
		}
	}

	return c, nil
}
