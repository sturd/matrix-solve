package matrix_solve

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewMatrix(t *testing.T) {
	a, err := NewMatrix("{{1, 0, -1,2},{0,3,1,-1},{2,4,0,3},{-3,1,-1,2}}")
	assert.Equal(t, nil, err)

	b, err := NewMatrix("{{1,2},{3,-1},{0,-2},{4, 1}}")
	assert.Equal(t, nil, err)

	c, err := NewMatrix("{{3,-2, 0,5},{1,0,-3,4}}")
	assert.Equal(t, nil, err)

	ab, err := a.Multiply(b)
	assert.Equal(t, nil, err)
	ab.NumRows()

	abc, err := ab.Multiply(c)
	assert.Equal(t, nil, err)
	val23, err := abc.Val(2,3)
	assert.Equal(t, nil, err)
	assert.Equal(t, 142.0, val23)
}